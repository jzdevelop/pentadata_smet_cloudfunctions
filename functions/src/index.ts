//import { isNull } from "util";

//bibliotecas necesariias para trabajar con firebase
const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const app = express();
const decimal = require('ascii-decimal');
//const JsonFind = require('json-find');
const xmlparser = require('express-xml-bodyparser');
app.use(express.json());
app.use(express.urlencoded());
app.use(xmlparser());
// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
const moment = require('moment-timezone');
admin.initializeApp();

app.get('/listar', (req: any, res: any) => {
    const db = admin.firestore();
    let datos: any = [];
    return db.collection('FrancorpDB').orderBy("fecha", "asc").get()
        .then((snapshot: any) => {
            snapshot.forEach((doc: any) => {
                datos.push({
                    fecha: parseInt(doc.data().fecha),
                    puntoVenta: doc.data().puntoVenta,
                    temperaturaAmbiente: parseFloat(doc.data().temperaturaAmbiente),
                    humedadAmbiente: parseFloat(doc.data().humedadAmbiente),
                    temperatura: parseFloat(doc.data().temperatura),
                    humedad: parseFloat(doc.data().humedad),
                    tiempoPuertaAbierta: parseFloat(doc.data().tiempoPuertaAbierta),
                    lugarMedicion: doc.data().lugarMedicion,
                });
            });
            res.json(datos);
        })
        .catch((err: any) => {
            res.send(err);
            console.log('Error getting documents', err);
        });
});


app.post('/tracking', (req: any, res: any) => {
    const date = moment().tz("America/Bogota").format();
    const year = date.slice(0, 4);
    //console.log(year);
    const month = date.slice(5, 7);
    //console.log(month);
    const day = date.slice(8, 10);
    //console.log(day);
    const hour = date.slice(11, 13);
    //console.log(hour);
    const minutes = date.slice(14, 16);
    //console.log(minutes);
    const dateNow: string = year.toString() + month.toString() + day.toString() + hour.toString() + minutes.toString();
    const dateInNumber: number = parseInt(dateNow);
    //let data = `${req.body}`;
    const parseString = require('xml2js').parseString;
    let dataXml = `${req.body}`;
    let latitude = 0.0, longitude = 0.0, temperature = "";
    parseString(dataXml, function (err: any, result: any) {
        const JsonFind = require('json-find');
        const converter = require('hex2dec');
        const payload = JsonFind(result).findValues('payload').payload;
        let payloadHexaValue = String(payload[0]._);
        latitude = parseFloat(converter.hexToDec('0x' + payloadHexaValue.slice(4, 10))) * (90.0 / Math.pow(2, 23));
        if (latitude > 90.0) {
            latitude = latitude - 180.0;
        }
        longitude = parseFloat(converter.hexToDec('0x' + payloadHexaValue.slice(10, 16))) * (180.0 / Math.pow(2, 23));
        if (longitude > 180.0) {
            longitude = longitude - 360.0;
        }
        temperature = "" + payloadHexaValue.slice(16, 18) + "." + payloadHexaValue.slice(18, 20);
        //res.set('Content-Type', 'text/xml');
        //console.log(longitude);
    });
    admin.firestore().collection("TrackingBoteroSoto").add({
        // temperatura y medición son lo mismo solo que por error se puso medición inicialmente
        // y se debió poner temperatura ya que se van a medir más variables
        xml: "" + dataXml,
        date: dateInNumber,
        latitude: latitude,
        longitude: longitude,
        temperature: temperature
    }).then((r: any) => {
        //console.log(parseFloat(request.query.humedad).toFixed(4));
        console.log(r);
        //console.log(r + "request: " + request.query.fecha);
    }).catch((er: any) => {
        console.log(er);
        //console.log(er);
    });
    parseString(dataXml, function (err: any, result: any) {
        const JsonFind = require('json-find');
        const messageID = JsonFind(result).findValues('messageID').messageID;
        res.set('Content-Type', 'text/xml');
        let xmlResponse = `<?xml version="1.0" encoding="utf-8"?> <stuResponseMsg xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://cody.glpconnect.com/XSD/StuResponse_Rev1_0.xsd" deliveryTimeStamp="25/08/2009 21:00:00 GMT" correlationID="${messageID}">   <state>pass</state>   <stateMessage>Store OK</stateMessage> </stuResponseMsg>`;
        res.status(200).send(xmlResponse);
    });

});

app.post('/sigfox', (req: any, res: any) => {
    admin.firestore().collection("SigfoxTempHum").add({
        data: "" + JSON.stringify(req.body)
    }).then((r: any) => {
        res.status(200).send("ok");
    }).catch((err: any) => {
        res.send("failed");
    });
});

app.post('/energy', async (req: any, res: any) => {
    const db = admin.firestore();
    const data = "" + req.body;
    const id = data[0];
    let day = decimal(data[1]) - 48;
    let month = decimal(data[2]) - 48;
    let year = decimal(data[3]) - 48;
    let hour = decimal(data[4]) - 48;
    let minute = decimal(data[5]) - 48;
    const currentSensor1 = (decimal(data[6]) - 48) + ((decimal(data[7]) - 48) / 10.0);
    const currentSensor2 = (decimal(data[8]) - 48) + ((decimal(data[9]) - 48) / 10.0);
    const currentSensor3 = (decimal(data[10]) - 48) + ((decimal(data[11]) - 48) / 10.0);
    const tempSign = data[12];
    let auxMonth = "" + month;
    let auxDay = "" + day;
    let auxHour = "" + hour;
    let auxMinute = "" + minute;
    let temperature = (decimal(data[13]) - 48) + (decimal(data[14]) - 48) / 10.0;
    if (month < 10) {
        auxMonth = '0' + month;
    }
    if (day < 10) {
        auxDay = "0" + day;
    }
    if (hour < 10) {
        auxHour = "0" + hour;
    }
    if (minute < 10) {
        auxMinute = "0" + minute;
    }
    if (tempSign === '1') {
        temperature = (-1) * temperature;
    }
    const date = "" + year + "" + auxMonth + auxDay + auxHour + auxMinute;
    const totalCurrent = currentSensor1 + currentSensor2 + currentSensor3;
    const powerInKw = (220 * totalCurrent) / 1000.0;
    db.collection("EnergyDB").add({
        id: id,
        date: parseInt(date),
        dataRaw: req.body,
        current: totalCurrent,
        power: powerInKw,
        temperature: temperature,
        voltage: 220
    }).then((r: any) => {
        console.log(r);
        res.status(200).send('ok');
    }).catch((err: any) => {
        console.log(err);
        res.send("failed");
    });
    /* let dataToAdd: any = [];
    async function extractData(data: string, measure: string) {
        let newData = '';
        for (let i = 0; i < data.length; i++) {
            if (data.charAt(i) === ',' && data.charAt(i + 1) === '"') {
                break;
            }
            if (data.charAt(i) !== ',') {
                newData = newData + data.charAt(i);
            }
            else {
                if (measure === 'T') {
                    getTemperature(newData);
                }
                else {
                    getCurrent(newData);
                }
                newData = '';
            }
        }
        return true;
    }

    function getTemperature(dataTemperature: string) {
        let temperature = 0;
        if (dataTemperature.charAt(10) === '0') {
            temperature = -parseInt(dataTemperature.substring(11)) / 10.0;
        }
        else {
            temperature = parseInt(dataTemperature.substring(11)) / 10.0;
        }
        const extractedData = {
            measure: 'T',
            dataRaw: '' + req.body,
            date: parseInt(dataTemperature.substring(0, 10)),
            temperature: temperature
        };
        dataToAdd.push(extractedData);
        //console.log("Temperature: " + extractedData);
    }
    function getCurrent(dataCurrent: string) {
        //2110192220000001001
        const current: number = parseInt(dataCurrent.substring(10, 13)) / 100.0 + parseInt(dataCurrent.substring(13, 16)) / 100.0 + parseInt(dataCurrent.substring(16)) / 100.0;
        const extractedData = {
            measure: 'C',
            dataRaw: '' + req.body,
            date: parseInt(dataCurrent.substring(0, 10)),
            current: current,
            voltage: 220,
            power: (current * 220.0) / 1000.0
        };
        //console.log("current: " + extractedData.current);
        dataToAdd.push(extractedData);
    }
    async function addDataToFirestore() {
        await dataToAdd.forEach((element: any) => {
            db.collection("EnergyDB").add(element)
                .then((r: any) => {
                    console.log(r);
                }).catch((err: any) => {
                    res.send("failed");
                });
        });
        return true;
    }
    const data1 = await extractData(req.body.substring(1), req.body.charAt(0));
    const data2 = await addDataToFirestore();
    console.log('' + data1 + data2); */
   
});

app.post('/test', async (req: any, res: any) => {
    const db = admin.firestore();
    let dataToAdd: any = [];
    async function extractData(data: string, measure: string) {
        let newData = '';
        for (let i = 0; i < data.length; i++) {
            if (data.charAt(i) === ',' && data.charAt(i + 1) === '"') {
                break;
            }
            if (data.charAt(i) !== ',') {
                newData = newData + data.charAt(i);
            }
            else {
                if (measure === 'T') {
                    getTemperature(newData);
                }
                else {
                    getCurrent(newData);
                }
                newData = '';
            }
        }
        return true;
    }

    function getTemperature(dataTemperature: string) {
        let temperature = 0;
        if (dataTemperature.charAt(10) === '0') {
            temperature = -parseInt(dataTemperature.substring(11)) / 10.0;
        }
        else {
            temperature = parseInt(dataTemperature.substring(11)) / 10.0;
        }
        const extractedData = {
            dataRaw: '' + req.body,
            date: parseInt(dataTemperature.substring(0, 10)),
            temperature: temperature
        };
        dataToAdd.push(extractedData);
        //console.log("Temperature: " + extractedData);
    }
    function getCurrent(dataCurrent: string) {
        //2110192220000001001
        const current: number = parseInt(dataCurrent.substring(10, 13)) / 100.0 + parseInt(dataCurrent.substring(13, 16)) / 100.0 + parseInt(dataCurrent.substring(16)) / 100.0;
        const extractedData = {
            dataRaw: '' + req.body,
            date: parseInt(dataCurrent.substring(0, 10)),
            current: '' + current,
            voltage: 220,
            power: (current * 220.0) / 1000.0
        };
        //console.log("current: " + extractedData.current);
        dataToAdd.push(extractedData);
    }
    async function addDataToFirestore() {
        await dataToAdd.forEach((element: any) => {
            db.collection("test").add(element)
                .then((r: any) => {
                    console.log(r);
                }).catch((err: any) => {
                    res.send("failed");
                });
        });
        return true;
    }
    const data1 = await extractData(req.body.substring(1), req.body[1]);
    const data2 = await addDataToFirestore();
    console.log('' + data1 + data2);
    res.status(200).send('ok');

});

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
//función a para obtener get request para agregar datos a la base de datos
export const addData = functions.https.onRequest((request: any, response: any) => {
    const date = moment().tz("America/Bogota").format();
    const year = date.slice(0, 4);
    //console.log(year);
    const month = date.slice(5, 7);
    //console.log(month);
    const day = date.slice(8, 10);
    //console.log(day);
    const hour = date.slice(11, 13);
    //console.log(hour);
    const minutes = date.slice(14, 16);
    //console.log(minutes);
    const dateNow: string = year.toString() + month.toString() + day.toString() + hour.toString() + minutes.toString();
    const dateInNumber: number = parseInt(dateNow);
    //Agrega los datos a firestore
    //necesita obtener el nombre de la colección donde va a guardar los datos y el punto de venta
    return admin.firestore().collection(request.query.dataBase).add({
        // temperatura y medición son lo mismo solo que por error se puso medición inicialmente
        // y se debió poner temperatura ya que se van a medir más variables
        temperatura: parseFloat(request.query.temperatura),
        fecha: dateInNumber,
        humedad: parseFloat(request.query.humedad),
        puntoVenta: request.query.puntoVenta,
        tiempoPuertaAbierta: parseFloat(request.query.tiempoPuertaAbierta),
        temperaturaAmbiente: parseFloat(request.query.temperaturaAmbiente),
        humedadAmbiente: parseFloat(request.query.humedadAmbiente),
        lugarMedicion: request.query.lugarMedicion,
        timestamp: date.slice(0, 19).replace("T", " ")
    }).then((r: any) => {
        //console.log(parseFloat(request.query.humedad).toFixed(4));
        response.send(r);
        //console.log(r + "request: " + request.query.fecha);
    }).catch((er: any) => {
        response.send(er);
        //console.log(er);
    });

});

export const pentadata = functions.https.onRequest(app);
//función para detectar los cambios en realtime database e inmediatamente enviarlos a forestore

export const sendNotification = functions.firestore
    .document('FrancorpAlertas/{id}').onWrite((change: any, context: any) => {
        function getFormatedPuntoVenta(puntoVenta: any) {
            let formatedString: string = "" + puntoVenta;
            formatedString = formatedString.substr(0, 1).toUpperCase() + formatedString.substr(1, formatedString.length - 1);
            for (let i = 0; i < formatedString.length; i++) {
                if (formatedString[i] == "_") {
                    formatedString = formatedString.slice(0, i) + " " + formatedString.slice(i + 1, i + 2).toUpperCase() + formatedString.slice(i + 2);
                }
            }
            return formatedString;
        }
        const messageInvitado = {
            notification: {
                title: 'Pentadata',
                body: getFormatedPuntoVenta(change.after.data().puntoVenta),
            },
            android: {
                priority: 'high',
                notification: {
                    title: "Pentadata",
                    body: "La " + change.after.data().zonaMedicion + " de " + getFormatedPuntoVenta(change.after.data().puntoVenta) + " Tiene problemas",
                    clickAction: "SPLASHACTIVITY",
                    sound: "fire_alarm",
                    channel_id: "CHANNEL_ID",
                    icon: "logo_pentadata"
                }
            },
            topic: change.after.data().puntoVenta
        };
        /* const messageAdmin = {
            notification: {
                title: 'SMET',
                body: 'Frisby Villa Grande Tiene Problemas',
            },
            android: {
                priority: 'high',
                notification: {
                    title: "SMET",
                    body: "Frisby Villa grande Tiene problemas",
                    click_action: "SPLASHACTIVITY",
                    sound: "fire_alarm"
                    //channel_id: "SmetServicesChannel"
                }
            },
            topic: "VillaGrande"
        }; */
        // Send a message to devices subscribed to the provided topic.
        return admin.messaging().send(messageInvitado)
            .then((response: any) => {
                // Response is a message ID string.
                console.log('Successfully sent message:', response);
            })
            .catch((error: any) => {
                console.log('Error sending message:', error);
            });
    });

// 



/* export const sendPushNotificaction = functions.database.ref('/alerta/message')
    .onWrite((change, context) => {
        //importa las librerias para obtener la fecha
        const moment = require('moment-timezone');
        const date = moment().tz("America/Bogota").format();
        //obtiene cada dato por separado de la fecha
        const year = date.slice(0, 4);
        //console.log(year);
        const month = date.slice(5, 7);
        //console.log(month);
        const day = date.slice(8, 10);
        //console.log(day);
        const hour = date.slice(11, 13);
        //console.log(hour);
        const minutes = date.slice(14, 16);
        //console.log(minutes);
        const dateNow: string = year.toString() + month.toString() + day.toString() + hour.toString() + minutes.toString();
        const dateInNumber: number = parseInt(dateNow);
        // se crea la instancia a la colección
        const userDoc = admin.firestore().collection('AlertasFrisby');
        const sensorId: string = "0x28, 0x40, 0x3, 0x98, 0x5, 0x0, 0x0, 0xED";
        // Only edit data when it is first created.
        if (!change.after.exists()) {
            return userDoc.id;
        }
        // Get the user object with the new changes,
        // as opposed to its value before the edit
        const userData = change.after.val();
        const topic = 'frisby';
        const message = {
            android: {

                notification: {
                    title: "SMET",
                    body: "Frisby Villa grande Tiene problemas",
                    click_action: "ALERTACTIVITY",
                    sound: "fire_alarm"
                }
            }
            //topic : topic
        };
        //agrega a la colección de alertas la fecha donde fue la alerta
        userDoc.add({
            medicion: parseFloat(userData),
            fecha: dateInNumber,
            sensor: sensorId,
            anio: parseInt(year),
            mes: parseInt(month),
            dia: parseInt(day),
            hora: parseInt(hour),
            minutes: parseInt(minutes)
        }).then((response) => {
            console.log('Successfully push data:', response);
        }).catch((error) => {
            console.log('Error pushing data:', error);
        });
        // envia la notifiación push a todos los dipositivos que están subscritos al topic
        return admin.messaging().sendToTopic(topic, message.android)
            .then((response) => {
                // Response is a message ID string.
                console.log('Successfully sent message:', response);
            }).catch((error) => {
                console.log('Error sending message:', error);
            });
    });
 */